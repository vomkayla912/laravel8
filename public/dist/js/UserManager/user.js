$(function () {
    fetch();
    $('#search').on('keyup', _.debounce(function () {
        var keyword = $('input[name=keyword]').val();
        keyword ? search(keyword) : fetch();
    }, 1000));
    $('.admin').addClass('menu-is-opening menu-open');
    $('#btnadd').click(function () {
        $('#mdadduser').modal('show');
        $('#adduser')[0].reset();
        $('.form-reset').removeClass('is-invalid').removeClass('is-valid');
        $('.error-reset').empty();
    });
    $.ajaxSetup({
        headers:
            {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    $('#edituser').submit(function () {
        event.preventDefault();
        $('#edituser').LoadingOverlay("show");
        var formData = $(this).serialize();
        var page = $('#page').val();
        var id = $('input[name=id]').val();
        $.ajax({
            url: '/Admin/Update-User/' + id,
            type: 'post',
            data: formData,
            success: function (data) {
                $('#edituser').LoadingOverlay("hide", true);
                $('#mdedituser').modal('hide');
                page ? fetch(page) : fetch();
                $.growl.notice({title: "Success", message: "Update User Complete!"});
            },
            error: function (data) {
                $('#edituser').LoadingOverlay("hide", true);

                var errorname = data.responseJSON.errors.name;
                var erroremail = data.responseJSON.errors.email;
                var errorpassword = data.responseJSON.errors.password;
                var errorcomfirm = data.responseJSON.errors.confirmpassword;

                errorname ? $('.form-name').addClass('is-invalid') : ($('.form-name').removeClass('is-invalid'), $('.form-name').addClass('is-valid'));
                $('.error-name').empty().append(errorname);

                erroremail ? $('.form-email').addClass('is-invalid') : ($('.form-email').removeClass('is-invalid'), $('.form-email').addClass('is-valid'));
                $('.error-email').empty().append(erroremail);

                errorpassword ? $('.form-password').addClass('is-invalid') : ($('.form-password').removeClass('is-invalid'), $('.form-password').addClass('is-valid'));
                $('.error-password').empty().append(errorpassword);

                errorcomfirm ? $('.form-confirmpassword').addClass('is-invalid') : ($('.form-confirmpassword').removeClass('is-invalid'), $('.form-confirmpassword').addClass('is-valid'));
                $('.error-comfirm').empty().append(errorcomfirm);
            }
        })
    });
    $('#adduser').submit(function () {
        event.preventDefault();
        $('#adduser').LoadingOverlay("show");
        var formData = $(this).serialize();
        var page = $('#page').val();
        $.ajax({
            url: "/Admin/Add-User",
            type: 'post',
            data: formData,
            success: function (response) {
                fetch();
                $('#adduser').LoadingOverlay("hide", true);
                $('#mdadduser').modal('hide');
                $('#adduser')[0].reset();
                $.growl.notice({title: "Success", message: "Add User Complete!"});
            },
            error: function (data) {
                $('#adduser').LoadingOverlay("hide", true);

                var errorname = data.responseJSON.errors.name;
                var erroremail = data.responseJSON.errors.email;
                var errorpassword = data.responseJSON.errors.password;
                var errorcomfirm = data.responseJSON.errors.confirmpassword;

                errorname ? $('.form-name').addClass('is-invalid') : ($('.form-name').removeClass('is-invalid'), $('.form-name').addClass('is-valid'));
                $('.error-name').empty().append(errorname);

                erroremail ? $('.form-email').addClass('is-invalid') : ($('.form-email').removeClass('is-invalid'), $('.form-email').addClass('is-valid'));
                $('.error-email').empty().append(erroremail);

                errorpassword ? $('.form-password').addClass('is-invalid') : ($('.form-password').removeClass('is-invalid'), $('.form-password').addClass('is-valid'));
                $('.error-password').empty().append(errorpassword);

                errorcomfirm ? $('.form-confirmpassword').addClass('is-invalid') : ($('.form-confirmpassword').removeClass('is-invalid'), $('.form-confirmpassword').addClass('is-valid'));
                $('.error-comfirm').empty().append(errorcomfirm);
            }
        })
    });

});

function deletebyid(id) {
    var page = $('#page').val();
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/Admin/Delete-User/' + id,
                type: 'get',
                success: function (response) {
                    page ? fetch(page) : fetch();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                },
                error: function () {
                    Swal.fire(
                        'Not Deleted!',
                        'Current account not delete',
                        'waring'
                    )
                }
            });
        }
    })
}

function getbyid(id) {
    $('.form-reset').removeClass('is-invalid').removeClass('is-valid');
    $('.error-reset').empty();
    $('#mdedituser').modal('show');
    $.ajax({
        url: '/Admin/GetById/' + id,
        type: 'get',
        success: function (data) {
            $('.title-edit').html('<i class="fas fa-user-edit"></i> Edit ' + data.name + '');
            $('input[name=id]').val(data.id);
            $('input[name=name]').val(data.name);
            $('input[name=email]').val(data.email);
            $('input[name=password]').val('');
            $('input[name=confirmpassword]').val('');
        },
        error: function () {
            alert('no data');
        }
    })
}

function paginate() {
    $('.pagination a').click(function (e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        fetch(page);
    });
}

function fetch(page) {
    $.ajax({
        url: '/Admin/User-Fetch?page=' + page,
        type: 'get',
        success: function (data) {
            $('#tbody').html(data);
            $('#page').val(page);
            paginate();
            checkitem(page);
        },
        error: function () {
            alert('No data');
        }
    })
}

function btnsearch() {
    var keyword = $('input[name=keyword]').val();
    keyword ? search(keyword) : fetch();
}

function unactiveuser(id) {
    var page = $('#page').val();
    $.ajax({
        url: '/Admin/UnActive-User/' + id,
        type: 'get',
        success: function () {
            page ? fetch(page) : fetch();
        },
        error: function () {
            $.growl.error({message: "Not unactive this account!"});
        }
    })
}

function activeuser(id) {
    var page = $('#page').val();
    $.ajax({
        url: '/Admin/Active-User/' + id,
        type: 'get',
        success: function () {
            page ? fetch(page) : fetch();
        }
    });
}

function search(keyword) {
    $.ajax({
        url: "/Admin/Search-User",
        type: 'get',
        data: {keyword: keyword},
        success: function (data) {
            $('#tbody').html(data);
        }
    });
}

function checkitem(page) {
    var count = $('.item-data').length;
    if (count == 0) {
        fetch(page - 1);
    }
}
