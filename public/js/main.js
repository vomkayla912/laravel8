const METHOD_GET = 'GET';
const METHOD_POST = 'POST';
const METHOD_PUT = 'PUT';
const METHOD_DELETE = 'DELETE';

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function callAjax(url, method, data) {
    return $.ajax({
        url: url,
        type: method,
        data: data,
    });
}

function callAjaxSearch(url, method, keyword) {
    return $.ajax({
        url: url,
        type: method,
        data: {keyword: keyword},
    });
}

function deleteAlert(url) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            callAjax(url, METHOD_DELETE)
                .then(function (res) {
                    res.error ? Swal.fire(res.error) : (Swal.fire(res.success),getList());
                })
        }
    })
}

function showSuccessMsg(message) {
    $('#modal').modal('hide');
    toastr.success(message);
}

function loadPlugins() {
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    })
}

//
function getList() {
    let url = $('#url-get-list').val();
    callAjax(url)
        .then(function (response) {
            $('.table-data').html(response);
        })
}

// Show modal create
$(document).on('click', '.btn-add', function () {
    let url = $(this).data('url');
    callAjax(url)
        .then(function (response) {
            $('.modal-content').html(response);
            loadPlugins();
        })
});

// Show modal edit
$(document).on('click', '.btn-edit', function () {
    let url = $(this).data('url');
    callAjax(url)
        .then(function (response) {
            $('.modal-content').html(response);
            loadPlugins();
        })
});

// Pagination
$('body').on('click', '.pagination li a', function (e) {
    e.preventDefault();
    let url = $(this).attr('href');
    callAjax(url)
        .then(function (response) {
            $('.table-data').html(response);
        })
})
