function errors(data) {
     var  errorname = data.responseJSON.errors.name;
    var erroremail = data.responseJSON.errors.email;
    var errorpassword = data.responseJSON.errors.password;
    var errorcomfirm = data.responseJSON.errors.confirmpassword;

    errorname ? $('.form-name').addClass('is-invalid') : ($('.form-name').removeClass('is-invalid'), $('.form-name').addClass('is-valid'));
    $('.error-name').empty().append(errorname);

    erroremail ? $('.form-email').addClass('is-invalid') : ($('.form-email').removeClass('is-invalid'), $('.form-email').addClass('is-valid'));
    $('.error-email').empty().append(erroremail);

    errorpassword ? $('.form-password').addClass('is-invalid') : ($('.form-password').removeClass('is-invalid'), $('.form-password').addClass('is-valid'));
    $('.error-password').empty().append(errorpassword);

    errorcomfirm ? $('.form-confirmpassword').addClass('is-invalid') : ($('.form-confirmpassword').removeClass('is-invalid'), $('.form-confirmpassword').addClass('is-valid'));
    $('.error-comfirm').empty().append(errorcomfirm);
}
