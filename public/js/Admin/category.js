$(function () {
    fetch();
    $('#search').on('keyup', _.debounce(function () {
        var keyword = $('input[name=keyword]').val();
        keyword ? search(keyword) : fetch();
    }, 1000));

    $('.admin').addClass('menu-is-opening menu-open');

    $('#btnadd').click(function () {
        $('#mdaddcategory').modal('show');
        $('#addcategory')[0].reset();
        $('.form-reset').removeClass('is-invalid').removeClass('is-valid');
        $('.error-reset').empty();
    });
    $.ajaxSetup({
        headers:
            {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    $('#editcategory').submit(function () {
        event.preventDefault();
        $('#editcategory').LoadingOverlay("show");
        var formData = $(this).serialize();
        var page = $('#page').val();
        var id = $('input[name=id]').val();
        $.ajax({
            url: '/categories/update/' + id,
            type: 'post',
            data: formData,
            success: function (data) {
                $('#editcategory').LoadingOverlay("hide", true);
                $('#mdeditcategory').modal('hide');
                page ? fetch(page) : fetch();
                $.growl.notice({title: "Success", message: "Update Category Complete!"});
            },
            error: function (data) {
                $('#editcategory').LoadingOverlay("hide", true);
                errors(data);
            }
        })
    });
    $('#addcategory').submit(function () {
        event.preventDefault();
        $('#addcategory').LoadingOverlay("show");
        var formData = $(this).serialize();
        var page = $('#page').val();
        $.ajax({
            url: "/categories/add",
            type: 'post',
            data: formData,
            success: function (response) {
                fetch();
                $('#addcategory').LoadingOverlay("hide", true);
                $('#mdaddcategory').modal('hide');
                $('#addcategory')[0].reset();
                $.growl.notice({title: "Success", message: "Add Category Complete!"});
            },
            error: function (data) {
                $('#addcategory').LoadingOverlay("hide", true);

                errors(data);
            }
        })
    });

});

function deletebyid(id) {
    var page = $('#page').val();
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/categories/delete/' + id,
                type: 'get',
                success: function (response) {
                    page ? fetch(page) : fetch();
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                },
                error: function () {
                    Swal.fire(
                        'Not Deleted!',
                        'Current account not delete',
                        'waring'
                    )
                }
            });
        }
    })
}

function getbyid(id) {
    $('.form-reset').removeClass('is-invalid').removeClass('is-valid');
    $('.error-reset').empty();
    $('#mdeditcategory').modal('show');
    $.ajax({
        url: '/categories/show/' + id,
        type: 'get',
        success: function (data) {
            $('.title-edit').html('<i class="fas fa-user-edit"></i> Edit ' + data.name + '');
            $('input[name=id]').val(data.id);
            $('input[name=name]').val(data.name);
            $('input[name=description]').val(data.description);
        },
        error: function () {
            alert('no data');
        }
    })
}

function paginate() {
    $('.pagination a').click(function (e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        fetch(page);
    });
}

function fetch(page) {
    $.ajax({
        url: '/categories/list?page=' + page,
        type: 'get',
        success: function (data) {
            $('#tbody').html(data);
            $('#page').val(page);
            paginate();
            checkitem(page);
        },
        error: function () {
            alert('No data');
        }
    })
}

function btnsearch() {
    var keyword = $('input[name=keyword]').val();
    keyword ? search(keyword) : fetch();
}

function search(keyword) {
    $.ajax({
        url: "/categories/search",
        type: 'get',
        data: {keyword: keyword},
        success: function (data) {
            $('#tbody').html(data);
        }
    });
}

function checkitem(page) {
    var count = $('.item-data').length;
    if (count == 0) {
        fetch(page - 1);
    }
}


