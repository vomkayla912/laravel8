$(function () {
    getList();

    $('#search').on('keyup', _.debounce(function () {
        let keyword = $('input[name=keyword]').val();
        let url = $(this).data('url');
        callAjaxSearch(url, METHOD_GET, keyword)
            .then(function (response) {
                $('.table-data').html(response);
            })
    }, 1000));

    $('.admin').addClass('menu-is-opening menu-open');


    $(document).on('submit', '#edituser', function () {
        event.preventDefault();
        $('#edituser').LoadingOverlay("show");
        let url = $(this).data('url');
        var formData = $(this).serialize();
        callAjax(url, METHOD_POST, formData)
            .then(function (response) {
                $('#edituser').LoadingOverlay("hide", true);
                showSuccessMsg(response.success);
                getList();
            })
            .catch(function (data) {
                $('#edituser').LoadingOverlay("hide", true);
                errors(data);
            })
    });


    $(document).on('submit', '#adduser', function () {
        event.preventDefault();
        $('#adduser').LoadingOverlay("show");
        let url = $(this).data('url');
        let formData = $(this).serialize();
        callAjax(url, METHOD_POST, formData)
            .then(function (response) {
                $('#adduser').LoadingOverlay("hide", true);
                showSuccessMsg(response.success);
                getList();
            })
            .catch(function (data) {
                $('#adduser').LoadingOverlay("hide", true);
                errors(data);
            })
    });

    // $(document).on('click', '.btn-active', function () {
    //     let url = $(this).data('url');
    //     callAjax(url)
    //         .then(function (res) {
    //             getList();
    //             res.error ? toastr.error(res.error) : toastr.success(res.success);
    //         })
    // })
    //
    // $(document).on('click', '.btn-unactive', function () {
    //     let url = $(this).data('url');
    //     callAjax(url)
    //         .then(function (res) {
    //             getList();
    //             toastr.success(res.success);
    //         })
    // })

    $(document).on('click','.btn-delete',function (){
        let url = $(this).data('url');
        sweetAlert(url);
    })
});


function errors(data) {
    let errorname = data.responseJSON.errors.name;
    let erroremail = data.responseJSON.errors.email;
    let errorpassword = data.responseJSON.errors.password;
    let errorcomfirm = data.responseJSON.errors.confirmpassword;

    errorname ? $('.form-name').addClass('is-invalid') : ($('.form-name').removeClass('is-invalid'), $('.form-name').addClass('is-valid'));
    $('.error-name').empty().append(errorname);

    erroremail ? $('.form-email').addClass('is-invalid') : ($('.form-email').removeClass('is-invalid'), $('.form-email').addClass('is-valid'));
    $('.error-email').empty().append(erroremail);

    errorpassword ? $('.form-password').addClass('is-invalid') : ($('.form-password').removeClass('is-invalid'), $('.form-password').addClass('is-valid'));
    $('.error-password').empty().append(errorpassword);

    errorcomfirm ? $('.form-confirmpassword').addClass('is-invalid') : ($('.form-confirmpassword').removeClass('is-invalid'), $('.form-confirmpassword').addClass('is-valid'));
    $('.error-comfirm').empty().append(errorcomfirm);
}
